@extends('layouts.template')

@section('content')
    <style>
        th {
            text-align: center;
        }
    </style>
    <h1>Your Squaretown</h1>
    <a href="{{url('/areas/create')}}" class="btn btn-success">Create Area</a>
    <hr>
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr class="bg-info">
            <th>Name</th>
            <th>Type</th>
            <th>District</th>
            <th>Location</th>
            <th>Thumbs</th>
            <th colspan="3">Actions</th>
        </tr>
        </thead>
        <tbody>
        @unless (empty($areas))
            @foreach ($areas as $area)
                <tr align="center">
                    <td>{{ $area->name }}</td>
                    <td>{{ \App\Area::TYPES[$area->type] }}</td>
                    <td>{{ $area->district_id }}</td>
                    <td>{{ $area->grid_x }}:{{ $area->grid_y }}</td>
                    <td><img src="{{asset('img/area/'.$area->type.'.png')}}" height="35" width="30"></td>
                    <td><a href="{{url('areas',$area->id)}}" class="btn btn-primary">Visit</a></td>
                    <td><a href="{{route('areas.edit',$area->id)}}" class="btn btn-warning">Update</a></td>
                    <td>
                        {!! Form::open(['method' => 'DELETE', 'route'=>['areas.destroy', $area->id]]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        @endunless


        </tbody>

    </table>
@endsection
