@extends('layouts.template')

@section('content')
    <h1>Create Area</h1>
    {!! Form::open(['url' => 'areas']) !!}
    <div class="form-group">
        <label for="name">Name:
            <div hidden="hidden">{{ $name = $errors->first('name') }}</div>
            @unless(empty($name))
                <div class="btn btn-danger">
                    {{ $errors->first('name') }}
                </div>
            @endunless
        </label>
        {!! Form::text('name',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        @include('areas._type_dropdown')
    </div>
    <div class="form-group">
        @include('areas._districts_dropdown')
    </div>
    <div class="form-group">
        <label for="grid_x">Grid X:
            <div hidden="hidden">{{ $grid_x = $errors->first('grid_x') }}</div>
            @unless(empty($grid_x))
                <div class="btn btn-danger">
                    {{ $errors->first('grid_x') }}
                </div>
            @endunless
        </label>
        {!! Form::text('grid_x',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        <label for="grid_y">Grid Y:
            <div hidden="hidden">{{ $grid_y = $errors->first('grid_y') }}</div>
            @unless(empty($grid_y))
                <div class="btn btn-danger">
                    {{ $errors->first('grid_y') }}
                </div>
            @endunless
        </label>
        {!! Form::text('grid_y',null,['class'=>'form-control']) !!}
    </div>
    {!! Form::hidden('user_id', Auth::user()->id) !!}

    <div class="form-group">
        {!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
    </div>
    {!! Form::close() !!}
@stop
