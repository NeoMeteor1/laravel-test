@extends('layouts.template')

@section('content')
    <style>
        th {
            text-align: center;
        }
    </style>
    @if(!empty($district))
        <h1>Your Areas in {{ $district->name }} </h1>
    @else
        <h1>Your Areas in this District</h1>

    @endif

    <a href="{{url('/areas/create')}}" class="btn btn-success">Create Area</a>
    <hr>
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr class="bg-info">
            <th>Name</th>
            <th>Location</th>
            <th>Type</th>
            <th colspan="3">Actions</th>
        </tr>
        </thead>
        <tbody>

        @unless(empty($areas))
            @foreach($areas as $area)
                    <tr align="center">
                        <td>{{ $area->name }}</td>
                        <td>{{ $area->grid_x }}:{{ $area->grid_y }}</td>
                        <td>
                            {{ App\Area::prettyUC($area->type) }}
                            <img src="{{asset('img/area/'.$area->type.'.png')}}" height="40" width="40">
                        </td>
                        <td><a href="{{url('areas',$area->id)}}" name="visit_{{$area->id}}" class="btn btn-primary">Visit</a></td>
                        <td><a href="{{route('areas.edit',$area->id)}}" name="update_{{$area->id}}" class="btn btn-warning">Update</a></td>
                        <td>
                            @if($area->type == App\Area::CITY_HALL_TYPE)
                                {!! Form::open(['method' => 'DELETE', 'route'=>['areas.destroy', $area->id]]) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger', 'name' => 'delete_'.$area->id, 'disabled' => 'disabled']) !!}
                                {!! Form::close() !!}
                            @else
                                {!! Form::open(['method' => 'DELETE', 'route'=>['areas.destroy', $area->id]]) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger', 'name' => 'delete_'.$area->id]) !!}
                                {!! Form::close() !!}
                            @endif
                        </td>
                    </tr>
            @endforeach
        @endunless
        </tbody>

    </table>

    <a href="{{ url('/')}}" class="btn btn-success">Back</a>

@endsection
