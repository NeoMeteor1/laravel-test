@extends('layouts.template')
@section('content')
    <h1>Update Area</h1>
    {!! Form::model($area,['method' => 'PATCH','route'=>['areas.update',$area->id]]) !!}

        @if($area->type == App\Area::CITY_HALL_TYPE)
            <div class="form-group">
            {!! Form::label('Name:', 'Name:') !!}
            {!! Form::text('city_hall_name',null,['class'=>'form-control','placeholder'=>'City Hall','readonly'=>'readonly']) !!}
            </div>
        @else
            <div class="form-group">
            {!! Form::label('Name:', 'Name:') !!}
            {!! Form::text('name',null,['class'=>'form-control' ]) !!}
                {{ $errors->first('name') }}
            </div>
        @endif

    @unless($area->type == App\Area::CITY_HALL_TYPE)
            @include('areas._type_dropdown')
    {{ $errors->first('type') }}
    @endunless
    <div class="form-group">
        @include('areas._districts_dropdown')
        {{ $errors->first('district') }}
    </div>
    <div class="form-group">
        {!! Form::label('Grid X Location:','Grid X Location:') !!}
        {!! Form::text('grid_x',null,['class'=>'form-control']) !!}
        {{ $errors->first('grid_x') }}
    </div>
    <div class="form-group">
        {!! Form::label('Grid Y Location:', 'Grid Y Location:') !!}
        {!! Form::text('grid_y',null,['class'=>'form-control']) !!}
        {{ $errors->first('grid_y') }}
    </div>
    <div class="form-group">
        {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
    </div>
    {!! Form::close() !!}
@stop
