<div class="form-group">
    <label for="district_id">District: </label>
    <select class="form-control"name="district_id">
        <option value=0>None</option>
        @foreach(App\District::all()->where('user_id',Auth::user()->id) as $district)
            @if(isset($area) && ($district->id == $area->district_id))
                <option selected="selected" name="{{ $district->id }}" value="{{ $district->id }}">{{ $district->name }} - {{ App\District::prettyUC($district->type) }}</option>
            @else
                <option name="{{ $district->id }}" value="{{ $district->id }}">{{ $district->name }}  - {{ App\District::prettyUC($district->type) }}</option>
            @endif
        @endforeach
    </select>
</div>