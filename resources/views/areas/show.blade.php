@extends('layouts.template')

@section('content')
    <h1>{{$area->name}}</h1>

    <form class="form-horizontal">
        <div align="center" class="form-group">
            <label for="image" class="col-sm-2 control-label"></label>
            <div class="col-sm-10">
                <img src="{{asset('img/area/'.$area->type.'.png')}}" height="180" width="180" class="img-rounded">
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="name" placeholder="{{$area->name}}" readonly>
            </div>
        </div>
        <div class="form-group">
            <label for="type" class="col-sm-2 control-label">Type</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="type" placeholder="{{ App\Area::prettyUC($area->type) }}" readonly>
            </div>
        </div>
        <div class="form-group">
            <label for="district" class="col-sm-2 control-label">District</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="district" placeholder="{{$area->district_id}}" readonly>
            </div>
        </div>
        <div class="form-group">
            <label for="location" class="col-sm-2 control-label">Location</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="location" placeholder="{{ $area->grid_x }}:{{ $area->grid_y }}" readonly>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">

                @if($area->district_id == 0)
                    <a href="{{ url('districts') }}" class="btn btn-primary">Back</a>
                @else
                    <a href="{{ url('district',$area->district_id)}}" class="btn btn-primary">Back</a>
                @endif
            </div>
        </div>
    </form>
@stop
