<div class="form-group">
    <label for="type">Type: </label>
    <select class="form-control" name="type">
        <option value="{{ App\Area::APARTMENT_TYPE }}">Apartment</option>
        <option value="{{ App\Area::BUSINESS_TYPE }}">Business</option>
        <option value="{{ App\Area::CAFE_TYPE }}">Cafe</option>
        <option value="{{ App\Area::FACTORY_TYPE }}">Factory</option>
        <option value="{{ App\Area::HOUSE_TYPE }}">House</option>
        <option value="{{ App\Area::MEDICAL_OFFICE_TYPE }}">Medical Office</option>
        <option value="{{ App\Area::RESTAURANT_TYPE }}">Restaurant</option>
        <option value="{{ App\Area::SCHOOL_TYPE }}">School</option>
        <option value="{{ App\Area::STORE_TYPE }}">Store</option>
        <option value="{{ App\Area::THEATRE_TYPE }}">Theatre</option>
    </select>
</div>
