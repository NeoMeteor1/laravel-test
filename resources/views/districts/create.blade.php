@extends('layouts.template')

@section('content')
    <h1>Create District</h1>
    {!! Form::open(['url' => 'districts']) !!}
    <div class="form-group">
        {!! Form::label('Name', 'Name:') !!}
        {!! Form::text('name',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        @include('districts._type_dropdown')
    </div>
    {!! Form::hidden('user_id', Auth::user()->id) !!}

    <div class="form-group">
        {!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
    </div>
    {!! Form::close() !!}
@stop
