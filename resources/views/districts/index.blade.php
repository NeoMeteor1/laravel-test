@extends('layouts.template')

@section('content')
    <style>
        th {
            text-align: center;
        }
    </style>

    <h1>Your Districts in Squaretown</h1>
        {{--{!! $map->name !!}</h1>--}}
    <a href="{{url('/districts/create')}}" class="btn btn-success">Create District</a>
    <hr>
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr class="bg-info">
            <th>Name</th>
            <th>Type</th>
            <th colspan="3">Actions</th>
        </tr>
        </thead>
        <tbody>
        @unless (empty($districts))
            @foreach ($districts as $district)
                <tr align="center">
                    <td>{{ $district->name }}</td>
                    <td>
                        {{ App\District::prettyUC($district->type) }}
                        <img src="{{asset('img/district/'.$district->type.'.png')}}" height="40" width="40"></td>
                    <td><a href="{{url('district', array('district_id' => $district->id))}}" name="Visit {{$district->name}}" class="btn btn-primary">Visit</a></td>
                    <td><a href="{{route('districts.edit',$district->id)}}" name="Update {{$district->name}}" class="btn btn-warning">Update</a></td>
                    <td>
                        {!! Form::open(['method' => 'DELETE', 'route'=>['districts.destroy', $district->id]]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger', 'name' => 'Delete '.$district->name]) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        @endunless
        </tbody>
    </table>

    @unless (empty($areas))

        <h1>Areas without a District</h1>
        <a href="{{url('/areas/create')}}" class="btn btn-success">Create Area</a>
        <hr>
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr class="bg-info">
                <th>Name</th>
                <th>Type</th>
                <th colspan="3">Actions</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($areas as $area)
                    @if($area->district_id == 0)
                        <tr align="center">
                            <td>{{ $area->name }}</td>
                            <td>
                                {{ \App\Area::prettyUC($area->type) }}
                                <img src="{{asset('img/area/'.$area->type.'.png')}}" height="40" width="40">
                            </td>
                            <td><a href="{{url('areas',$area->id)}}" name="Visit {{$area->name}}" class="btn btn-primary">Visit</a></td>
                            <td><a href="{{route('areas.edit',$area->id)}}" name="Update {{$area->name}}" class="btn btn-warning">Update</a></td>
                            <td>
                                @if($area->type == App\Area::CITY_HALL_TYPE)
                                    {!! Form::open(['method' => 'DELETE', 'route'=>['areas.destroy', $area->id]]) !!}
                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger', 'name' => 'Delete '.$area->name, 'disabled' => 'disabled']) !!}
                                    {!! Form::close() !!}
                                @else
                                    {!! Form::open(['method' => 'DELETE', 'route'=>['areas.destroy', $area->id]]) !!}
                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger', 'name' => 'Delete '.$area->name]) !!}
                                    {!! Form::close() !!}
                                @endif
                            </td>
                        </tr>
                    @endif
                @endforeach

                {{ $areas->render() }}
            </tbody>
        </table>
    @endunless

@endsection
