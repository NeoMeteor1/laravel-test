@extends('layouts.template')

@section('content')
    <h1>Update District</h1>
    {!! Form::model($district,['method' => 'PATCH','route'=>['districts.update',$district->id]]) !!}
    <div class="form-group">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name',null,['class'=>'form-control']) !!}
        {{ $errors->first('name') }}
    </div>
    <div class="form-group">
        @include('districts._type_dropdown')
        {{ $errors->first('type') }}
    </div>
    <div class="form-group">
        {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
    </div>
    {!! Form::close() !!}
@stop
