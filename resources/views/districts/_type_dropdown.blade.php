<div class="form-group">
    <label for="type">Type:</label>
    <select class="form-control" name="type">
        <option value="{{ App\District::BUSINESS_TYPE }}">Business</option>
        <option value="{{ App\District::COMMERCE_TYPE }}">Commerce</option>
        <option value="{{ App\District::INDUSTRIAL_TYPE }}">Industrial</option>
        <option value="{{ App\District::RESIDENTIAL_TYPE }}">Residential</option>
        <option value="{{ App\District::TOURIST_TYPE }}">Tourist</option>
        <option value="{{ App\District::UNDEVELOPED_TYPE }}">Undeveloped</option>
    </select>
</div>