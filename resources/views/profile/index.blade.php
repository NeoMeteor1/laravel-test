@extends('layouts.template')

@section('content')
    @if(Session::has('profile_created'))
        <div class="alert alert-success">{{ Session::get('profile_created') }}</div>
    @endif
    <h1>{{$user->first_name}}'s Profile Page</h1>
    <form class="form-horizontal">
        <div align="center" class="form-group">
            <label for="profile_pic" class="col-sm-2 control-label"></label>
            <div class="col-sm-10">
                <img src="{{asset( $profile->profile_pic )}}" height="200" width="200" class="img-rounded">
            </div>
        </div>
        <div class="form-group">
            <label for="first_name" class="col-sm-2 control-label">First Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="first_name" placeholder="{{$user->first_name}}" readonly>
            </div>
        </div>
        <div class="form-group">
            <label for="last_name" class="col-sm-2 control-label">Last Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="last_name" placeholder="{{$user->last_name}}" readonly>
            </div>
        </div>
        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">Email Address</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="email" placeholder="{{ $user->email }}" readonly>
            </div>
        </div>
        <div class="form-group">
            <label for="website" class="col-sm-2 control-label">Website Link</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="website" placeholder="{{$profile->website}}" readonly>
            </div>
        </div>
        <div class="form-group">
            <label for="description" class="col-sm-2 control-label">Bio</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="descritption" placeholder="{{ $profile->description }}" readonly>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                    <a href="{{ url('districts') }}" class="btn btn-primary">Back</a>
                    <a href="{{ url('profile.edit')}}" class="btn btn-primary">Edit</a>
            </div>
        </div>
    </form>
@stop
