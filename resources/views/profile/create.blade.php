@extends('layouts.template')

@section('content')
    <h1>Let's Set Up Your Profile!</h1>
    {!! Form::open(['url' => 'profile','files'=>true]) !!}
    <div class="form-group">
        <label for="first_name">First Name:
            <div hidden="hidden">{{ $name = $errors->first('first_name') }}</div>
            @unless(empty($first_name))
                <div class="btn btn-danger">
                    {{ $errors->first('first_name') }}
                </div>
            @endunless
        </label>
        {!! Form::text('first_name',Auth::user()->first_name,['class'=>'form-control']) !!}
    </div>

    <div class="form-group">
        <label for="last_name">Last Name:
            <div hidden="hidden">{{ $name = $errors->first('last_name') }}</div>
            @unless(empty($last_name))
                <div class="btn btn-danger">
                    {{ $errors->first('last_name') }}
                </div>
            @endunless
        </label>
        {!! Form::text('last_name',Auth::user()->last_name,['class'=>'form-control']) !!}
    </div>

    <div class="form-group">
        <label for="email">Email Address:
            <div hidden="hidden">{{ $email = $errors->first('email') }}</div>
            @unless(empty($email))
                <div class="btn btn-danger">
                    {{ $errors->first('email') }}
                </div>
            @endunless
        </label>
        {!! Form::text('email',Auth::user()->email,['class'=>'form-control']) !!}
    </div>

    <div class="form-group">
        <label for="website">Your Website:
            <div hidden="hidden">{{ $website = $errors->first('website') }}</div>
            @unless(empty($website))
                <div class="btn btn-danger">
                    {{ $errors->first('website') }}
                </div>
            @endunless
        </label>
        {!! Form::text('website',null,['class'=>'form-control']) !!}
    </div>

    <div class="form-group">
        <label for="description">Your Bio:
            <div hidden="hidden">{{ $description = $errors->first('description') }}</div>
            @unless(empty($description))
                <div class="btn btn-danger">
                    {{ $errors->first('description') }}
                </div>
            @endunless
        </label>
        {!! Form::text('description','Enter a short bio here! (Max length is 500 characters)',['class'=>'form-control', ]) !!}
    </div>

    <div class="form-group">
        <label for="profile_pic">Upload Profile Image:
            <div hidden="hidden">{{ $profile_pic = $errors->first('profile_pic') }}</div>
            @unless(empty($profile_pic))
                <div class="btn btn-danger">
                    {{ $errors->first('profile_pic') }}
                </div>
            @endunless
        </label>
        {!! Form::file('image')!!}
    </div>

    <div class="form-group">
        <label for="notifications">Email Notifications:
            <div hidden="hidden">{{ $notifications = $errors->first('notifications') }}</div>
            @unless(empty($notifications))
                <div class="btn btn-danger">
                    {{ $errors->first('notifications') }}
                </div>
            @endunless
        </label>
            <select class="form-group" name="notifications">
                <option value="{{ App\Profile::NOTIFICATIONS_ON }}">On</option>
                <option value="{{ App\Profile::NOTIFICATIONS_OFF }}">Off</option>
            </select>
    </div>

    <div class="form-group">
        {!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
    </div>
    {!! Form::close() !!}
@stop
