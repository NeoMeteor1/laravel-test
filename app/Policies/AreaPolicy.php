<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class AreaPolicy
 * @package App\Policies
 */
class AreaPolicy
{
    use HandlesAuthorization;
    /**
     * Create a new policy instance.
     *
     */
    public function __construct()
    {
        //
    }
    /**
     * Determine if the given post can be updated by the user.
     *
     * @param $user
     * @param $area
     * @return bool
     */
    public function update(User $user, Area $area)
    {
        return $user->id === $area->user_id;
    }
    /**
     * Determine if the given post can be delete by the user.
     *
     * @param $user
     * @param $area
     * @return bool
     */
    public function delete(User $user, Area $area)
    {
        return $user->id === $area->user_id;
    }
}
