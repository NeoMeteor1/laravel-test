<?php

namespace App\Providers;

//use App\Http\Requests;
use Validator;
use App\Area;
use App\Http\Requests\AreaCreateRequest;
use Illuminate\Support\ServiceProvider;

/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('gridlocation', function ($attribute, $value, $parameters, $validator) {

            $district_id = array_get($validator->getData(), 'district_id', null);
            $grid_x = array_get($validator->getData(), 'grid_x', null);
            $grid_y = array_get($validator->getData(), 'grid_y', null);
            $id = array_get($validator->getData(), 'id', null);
            $area = Area::uniqueGridLocation($id, $district_id, $grid_x, $grid_y);
            if (!isset($area->id)) {
                return true;
            }
            
            return false;
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
