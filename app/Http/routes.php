<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::get('Area', function () {
        Area::get();
    });
    Route::resource('areas', 'AreaController');
    Route::resource('districts', 'DistrictController');
    Route::resource('profile', 'ProfileController');
    Route::get('/', 'DistrictController@index');
    Route::get('district/0', 'DistrictController@index');
    Route::get('district/{district_id}', 'AreaController@district');
});
