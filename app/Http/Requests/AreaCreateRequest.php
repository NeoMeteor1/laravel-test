<?php

namespace App\Http\Requests;

/**
 * Class AreaCreateRequest
 * @package App\Http\Requests
 */
class AreaCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|regex:/^[A-Za-z\.\-\#\' \t]*$/i|max:255',
            'grid_x' => 'numeric|required_if:grid_y,6|gridlocation',
            'grid_y' => 'numeric',
            'profile_pic' => 'image',
        ];
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {

        return [
            'name.required' => 'Your Area has to have a name!',
            'grid_x.gridlocation' =>
                'The X::Y coordinates for this Area cannot match those of any other Area in this District',
            'grid_y.required' => 'I gotta have that "Y" coordinate!',

        ];
    }
}
