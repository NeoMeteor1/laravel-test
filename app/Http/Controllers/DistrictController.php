<?php

namespace App\Http\Controllers;

use App\District;
use App\Area;
use Auth as Auth;
use App\Http\Requests;
use App\Http\Requests\DistrictRequest;

/**
 * Class DistrictController
 * @package App\Http\Controllers
 */
class DistrictController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the district.
     *
     * @param int $district
     * @return mixed
     * @internal param $request
     */
    public function index($district = 0)
    {
        $conditions = [
            'user_id' => Auth::user()->id,
            'district_id' => $district,
        ];
        $areas = Area::where($conditions)->paginate(3);

        $districts = District::where('user_id', Auth::user()->id)->get();

        return view('districts.index', compact('districts', 'areas'));
    }
    /**
     * Show the form for creating a new district.
     *
     * @return mixed
     */
    public function create()
    {
        return view('districts.create');
    }
    /**
     * Store a newly created district in the database.
     *
     * @param $request
     * @return mixed
     */
    public function store(DistrictRequest $request)
    {
        $district = $request->all();
        District::create($district);
        return redirect('districts');
    }
    /**
     * Show the form for editing the specified district.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        $district=District::find($id);
        
        return view('districts.edit', compact('district'));
    }
    /**
     * Update the specified district in the database.
     *
     * @param  int  $id
     * @param  $request
     * @return mixed
     */
    public function update($id, DistrictRequest $request)
    {
        $districtUpdate = $request->all();
        $district = District::find($id);
        $district->update($districtUpdate);
        return redirect('districts');
    }
    /**
     * Remove the specified district from the database.
     *
     * @param  int  $id
     * @return mixed
     */
    public function destroy($id)
    {
        District::find($id)->delete();
        $conditions = [
            'user_id' => Auth::user()->id,
            'district_id' => $id,
        ];
        $areas = Area::where($conditions)->get();
        foreach ($areas as $area) {
            $area->district_id = 0;
            $area->save();
        }
        return redirect('districts');
    }
}
