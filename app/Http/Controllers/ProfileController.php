<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Events\ProfileCreation;
use Redirect;
use Session;
use App\Profile;
use App\Area;
use App\User;
use Auth;

/**
 * Class ProfileController
 * @package App\Http\Controllers
 */
class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * The User's Profile page.
     * @return mixed
     * @internal param $user
     * @internal param $profile
     */
    public function index()
    {
        $user = Auth::user();
        $profile = Profile::where('profile_id', $user->id)->first();

        return view('profile.index', compact('user', 'profile'));
    }
    /**
     * @return string
     */
    public function upload()
    {
        $file = array('image' => Input::file('image'));
        $rules = array('image' => 'image',);
        $validator = Validator::make($file, $rules);
        
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            if (!is_null(Input::file('image'))) {
                $destinationPath = 'img/profile/' . rand(11111, 99999);
                $extension = Input::file('image')->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;
                Input::file('image')->move($destinationPath, $fileName);
                Session::flash('success', 'Upload successfully');
                return $destinationPath . '/' . $fileName;
            } else {
                Session::flash('error', 'uploaded file is not valid');
                return 'img/profile/default/avatar.jpg';
            }
        }
    }
    /**
     * Show the form for creating a new Profile.
     *
     * @return mixed
     */
    public function create()
    {

        return view('profile.create');
    }
    /**
     * Store a newly created Profile in the database.
     *
     * @param $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
            'first_name' => 'required|alpha_num|max:255',
            'last_name'  => 'required|alpha_num|max:255',
            'email'      => 'required|email',
            'website'    => 'url',
            'profile_pic' => 'image',
            ],
            [
                'first_name.required' => 'Please Enter a first name!',
                'email.email'  => 'Not a valid Email address!!',
            ]
        );
        if ($validator->fails()) {
            $messages = $validator->messages();

            return redirect('profile/create')
                ->withErrors($messages)
                ->withInput();
        }
        $profile_pic = $this->upload();
        $info=$request->all();

        if (!is_file($profile_pic)) {
            $profile_pic = 'img/profile/default/avatar.jpg';
        }

        $profile = [
            'profile_id' => Auth::user()->id,
            'profile_pic' => $profile_pic,
            'description' => $info['description'],
            'website' => $info['website'],
            'notifications' => $info['notifications'],
        ];
        $userEdit = [
            'first_name' => $info['first_name'],
            'last_name'  => $info['last_name'],
            'email'      => $info['email'],
        ];

        $profile = Profile::create($profile);
        $user = User::where('id', $profile->profile_id)->first();
        $user->save($userEdit);
        $firstArea = AreaController::cityHall();
        event(new ProfileCreation($profile));

        return redirect('profile');
    }
    /**
     * Show the form for editing the User's Profile.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        $profile=Profile::find($id);
        $user = Auth::user();

        return view('profile.edit', compact('profile', 'user'));
    }
    /**
     * Update the specified Profile in the database.
     *
     * @param  int  $id
     * @param  $request
     * @return mixed
     */
    public function update($id, Request $request)
    {
        $profileUpdate=$request->all();
        $profile=Profile::find($id);
        $profile->update($profileUpdate);

        return view('profile');
    }
    /**
     * Remove the specified Profile from the database.
     *
     * @param  int  $id
     * @return mixed
     */
    public function destroy($id)
    {
        $area = Area::find($id);
        $area->delete();

        return redirect('district/' . $area->district_id);
    }
}
