<?php

namespace App\Http\Controllers;

use App\Area;
use App\District;
use Auth as Auth;
use App\Http\Requests\AreaCreateRequest;
use App\Http\Requests;

/**
 * Class AreaController
 * @package App\Http\Controllers
 */
class AreaController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the district.
     *
     * @param $areas
     * @param $district
     * @return mixed
     */
    public function index($areas = null, $district = null)
    {

        return view('areas.index', compact('areas', 'district'));
    }
    /**
     * This is to be used to create the 'city_hall' Area when a user first accesses their account.
     */
    public static function cityHall()
    {
        Area::firstOrCreate(array(
            'user_id' => Auth::user()->id,
            'name' => 'City Hall',
            'type' => 'city_hall',
            'grid_x' => 1,
            'grid_y' => 1,
        ));
    }
    /**
     * Display a listing of the Areas in a given District.
     *
     * @param $district_id
     * @return mixed
     * @internal param $request
     */
    public function district($district_id)
    {
        $areas = Area::areadistrict($district_id)->get();
        $district= District::find($district_id);
        return $this->index($areas, $district);
    }
    /**
     * Show the form for creating a new Area.
     *
     * @return mixed
     */
    public function create()
    {

        return view('areas.create');
    }
    /**
     * Store a newly created Area in the database.
     *
     * @param $request
     * @return mixed
     */
    public function store(AreaCreateRequest $request)
    {
        $area=$request->all();

        $area = Area::create($area);

        if ($area->district_id == 0) {
            return redirect('districts');
        } else {
            return redirect('district/' . $area->district_id);
        }

    }
    /**
     * Display the specified Area.
     *
     * @param  int  $id
     * @return mixed
     */
    public function show($id)
    {
        $area=Area::find($id);

        return view('areas.show', compact('area'));
    }
    /**
     * Show the form for editing the specified Area.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        $area=Area::find($id);

        return view('areas.edit', compact('area'));
    }
    /**
     * Update the specified Area in the database.
     *
     * @param  int  $id
     * @param  $request
     * @return mixed
     */
    public function update($id, AreaCreateRequest $request)
    {
        $areaUpdate=$request->all();
        $area=Area::find($id);
        $area->update($areaUpdate);

        return redirect('district/' . $area->district_id);
    }
    /**
     * Remove the specified Area from the database.
     *
     * @param  int  $id
     * @return mixed
     */
    public function destroy($id)
    {
        $area = Area::find($id);
        $area->delete();

        return redirect('district/' . $area->district_id);
    }
}
