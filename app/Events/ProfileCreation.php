<?php

namespace App\Events;

use App\Profile;
use Illuminate\Queue\SerializesModels;

/**
 * Class ProfileCreation
 * @package App\Events
 */
class ProfileCreation extends Event
{
    use SerializesModels;
    /**
     * @var $profile
     */
    public $profile;
    /**
     * Create a new event instance.
     *
     * @param Profile $profile
     */
    public function __construct(Profile $profile)
    {
        $this->profile = $profile;
    }
    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
