<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class District
 * @package App
 */
class District extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'type',
        'user_id',
        'map_id',
        'width',
        'height',
    ];
    const BUSINESS_TYPE             = 'business';
    const COMMERCE_TYPE             = 'commerce';
    const INDUSTRIAL_TYPE           = 'industrial';
    const RESIDENTIAL_TYPE          = 'residential';
    const TOURIST_TYPE              = 'tourist';
    const UNDEVELOPED_TYPE          = 'undeveloped';
    /**
     * Get all of the product's likes.
     */
    public function land()
    {
        return $this
            ->morphMany('Map', 'ownable');
    }
    /**
     * Get the Areas that belong to the District
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function area()
    {
        return $this
            ->hasMany('Area');
    }
    /**
     * Get the Areas that belong to the District
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this
            ->belongsToMany('User');
    }
    /**
     * Takes database table values and makes them pretty for the user
     * [Upper-cases and spaces words]
     *
     * @param $str
     * @return mixed
     */
    public static function prettyUC($str)
    {
        return ucwords(str_replace("_", " ", $str));
    }
}
