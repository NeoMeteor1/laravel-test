<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Map
 * @package App
 */
class Map extends Model
{
    /**
     * List of database fields that should not be edited
     *
     * @var array
     */
    protected $guarded = [
        'id', 'user_id'
    ];

    /**
     * Get all of the land (Area and District) models.
     */
    public function ownable()
    {
        return $this->morphTo();
    }
}
