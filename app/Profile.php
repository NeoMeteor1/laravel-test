<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Profile
 * @package App
 */
class Profile extends Model
{
    /**
     * List of database fields that should not be edited
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    /**
     * List of database fields that should only be visible to the App
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'profile_id',
    ];
    const NOTIFICATIONS_ON  = '1';
    const NOTIFICATIONS_OFF = '0';
}
