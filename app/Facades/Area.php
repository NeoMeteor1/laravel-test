<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Area
 * @package App\Facades
 */
class Area extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {

        return 'Area';
    }
}
