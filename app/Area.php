<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Area
 *
 * The Area model
 *
 * @package App
 */
class Area extends Model
{
    /**
     * List of database fields that should not be edited
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];
    /**
     * List of database fields that should only be visible to the App
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'user_id',
    ];
    const APARTMENT_TYPE        = 'apartment';
    const BANK_TYPE             = 'bank';
    const BUSINESS_TYPE         = 'business';
    const CAFE_TYPE             = 'cafe';
    const CITY_HALL_TYPE        = 'city_hall';
    const FACTORY_TYPE          = 'factory';
    const HOUSE_TYPE            = 'house';
    const MEDICAL_OFFICE_TYPE   = 'medical_office';
    const RESTAURANT_TYPE       = 'restaurant';
    const SCHOOL_TYPE           = 'school';
    const STORE_TYPE            = 'store';
    const THEATRE_TYPE          = 'theatre';
    /**
     * Get all of the Map's Area's.
     */
    public function land()
    {
        return $this
            ->morphMany('Map', 'ownable');
    }
    /**
     * Get the District that owns the Area
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function district()
    {
        return $this
            ->hasOne('App\District');
    }
    /**
     * Get the User that owns this Area
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {

        return $this
            ->hasOne('App\User');
    }
    /**
     * Query scope finding Areas based on District
     *
     * @param $query
     * @param $district_id
     * @return mixed
     */
    public function scopeAreaDistrict($query, $district_id)
    {
        return $query
            ->where('user_id', Auth::user()->id)
            ->where('district_id', $district_id);
    }

    /**
     * Determine if attempted grid placement is valid
     *
     * @param $query
     * @param $id
     * @param $district_id
     * @param $grid_x
     * @param $grid_y
     */
    public function scopeUniqueGridLocation($query, $id, $district_id, $grid_x, $grid_y)
    {
        if (!is_null($id)) {
            return $query->where('user_id', Auth::user()->id)
                ->where('id', '!=', $id)
                ->where('district_id', $district_id)
                ->where('grid_x', $grid_x)
                ->where('grid_y', $grid_y);
        } else {
            return $query
                ->where('user_id', Auth::user()->id)
                ->where('district_id', $district_id)
                ->where('grid_x', $grid_x)
                ->where('grid_y', $grid_y);
        }
    }
    /**
     * Takes database table values and makes them pretty for the user
     * [Upper-cases and spaces words]
     *
     * @param $str
     * @return mixed
     */
    public static function prettyUC($str)
    {
        return ucwords(str_replace("_", " ", $str));
    }
}
