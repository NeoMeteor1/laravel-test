<?php

namespace App\Jobs;

use App\User;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class SendAreaConfirmationEmail
 * @package App\Jobs
 */
class SendAreaConfirmationEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue;
    use SerializesModels;
    /**
     * @var User
     */
    protected $user;
    /**
     * Create a new job instance.
     *
     * @param  User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
    /**
     * Execute the job.
     *
     * @param  Mailer  $mailer
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        $mailer
            ->send('email.area_built_html', ['user' => $this->user], function ($m) {
                $m
                    ->from('Mayor@squaretown.com', 'Squaretown');
                $m
                    ->to($this->user->email, $this->user->first_name . ' ' . $this->user->last_name)
                    ->subject('New Area Created!');
            });
    }
}
