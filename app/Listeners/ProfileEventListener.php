<?php

namespace App\Listeners;

/**
 * Class ProfileEventListener
 * @package App\Listeners
 */
class ProfileEventListener
{
    /**
     * @param $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\ProfileCreation',
            'App\Listeners\ConfirmationEmail@handle'
        );

        $events->listen(
            'App\Events\ProfileCreation',
            'App\Listeners\ConfirmationMessage@handle'
        );
    }
}
