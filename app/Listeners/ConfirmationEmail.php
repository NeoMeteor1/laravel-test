<?php

namespace App\Listeners;

use App\Http\Requests;
use Auth;
use Mail;
use App\Events\ProfileCreation;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class ConfirmationEmail
 * @package App\Listeners
 */
class ConfirmationEmail implements ShouldQueue
{
    use InteractsWithQueue;
    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {

    }
    /**
     * Handle the event.
     *
     * @param  ProfileCreation  $event
     * @return void
     */
    public function handle(ProfileCreation $event)
    {
        if (true) {
            $this->release(30);
        }
        Mail::queue(['email.welcome_html', 'email.welcome_txt'], ['key' => 'value'], function ($message) {
            $message
                ->to(Auth::user()
                    ->email, Auth::user()
                        ->first_name . ' ' . Auth::user()
                        ->last_name)
                ->subject('Welcome to Squaretown!');
        });
    }
}
