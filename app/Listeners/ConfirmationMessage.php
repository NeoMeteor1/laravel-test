<?php

namespace App\Listeners;

use App\Events\ProfileCreation;
use Session;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class ConfirmationMessage
 * @package App\Listeners
 */
class ConfirmationMessage implements ShouldQueue
{
    use InteractsWithQueue;
    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {

    }
    /**
     * Handle the event.
     *
     * @param  ProfileCreation  $event
     * @return void
     */
    public function handle(ProfileCreation $event)
    {
        if (true) {
            $this->release(30);
        }
        Session::flash('profile_created', 'Your account has been successfully created!');
    }
}
