<?php

use Illuminate\Database\Seeder;
use App\District;

class DistrictsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        District::create([
            'user_id' => 1,
            'name' => 'Test District',
            'type' => 'undeveloped',
        ]);
    }
}
