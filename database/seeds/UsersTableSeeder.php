<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'first_name' => 'Default',
            'last_name' => 'User',
            'email' => 'Duser@testco.com',
            'password' => bcrypt('Secr3t'),
        ]);

    }
}
