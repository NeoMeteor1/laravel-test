<?php

use Illuminate\Database\Seeder;
use App\Profile;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Profile::create([
            'profile_id' => 1,
            'profile_pic' => 'img/profile/default/avatar.jpg',
            'description' => 'Test District',
            'website' => 'http://www.testco.com',
            'notifications' => '1',
        ]);
    }
}
