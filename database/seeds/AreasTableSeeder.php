<?php

use Illuminate\Database\Seeder;

class AreasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Area::create([
            'user_id' => 1,
            'district_id' => 0,
            'name' => 'City Hall',
            'type' => 'city_hall',
            'grid_x' => 0,
            'grid_y' => 0,
        ]);

        Area::create([
            'user_id' => 1,
            'district_id' => 0,
            'name' => 'Test Area',
            'type' => 'business',
            'grid_x' => 0,
            'grid_y' => 0,
        ]);

        Area::create([
            'user_id' => 1,
            'district_id' => 1,
            'name' => 'Inside District Area',
            'type' => 'tourist',
            'grid_x' => 0,
            'grid_y' => 0,
        ]);
    }
}
