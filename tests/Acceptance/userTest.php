<?php

namespace App;

use Illuminate\Foundation\Testing\DatabaseMigrations;

/**
 * Class TestUser
 * @package App
 */
class UserTest extends \TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();

        $this
            ->artisan('db:seed', [
                '--class' => 'UsersTableSeeder',
            ]);
    }

    public function testRegistration()
    {
        $this
            ->visit('/')
            ->see('New to Squaretown? Sign up for a free account!')
            ->click('Register')
            ->see('Register')
            ->type('Testguy', 'first_name')
            ->type('Testerson', 'last_name')
            ->type('testguy@testco.com', 'email')
            ->type('Secr3t', 'password')
            ->type('Secr3t', 'password_confirmation')
            ->press('Register')
            ->see('Let\'s Set Up Your Profile!')
            ->type('Test User Account', 'description')
            ->type('http://www.testco.com', 'website')
            ->press('Save')
            ->seePageIs('/profile');
    }

    public function testLoginLogout()
    {
        $this
            ->visit('/')
            ->see('Squaretown')
            ->type('Duser@testco.com', 'email')
            ->type('Secr3t', 'password')
            ->press('Login')
            ->see('Your Districts in Squaretown')
            ->click('Logout');
    }

    public function testResetPass()
    {
        $this
            ->visit('/')
            ->see('Forgot Your Password?')
            ->click('Reset')
            ->see('Reset Password')
            ->type('Duser@testco.com', 'email')
            ->press('Send Password Reset Link')
            ->see('We have e-mailed your password reset link!');
    }
}
