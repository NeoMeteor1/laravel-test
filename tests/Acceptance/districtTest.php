<?php

namespace App;

use Illuminate\Foundation\Testing\DatabaseMigrations;

class DistrictTest extends \TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();

        $this
            ->artisan('db:seed');

        $user = User::where('email', 'Duser@testco.com')->first();

        $this
            ->actingAs($user)
            ->visit('/')
            ->see('Your Districts in Squaretown');
    }
    public function testCreateDistrict()
    {
        $this
            ->click('Create District')
            ->see('Create District')
            ->type('Test Business District', 'name')
            ->select('business', 'type')
            ->press('Save');
    }

    public function testVisitDistrict()
    {
        $this
            ->click('Visit Test District')
            ->see('Your Areas in Test District ')
            ->see('Inside District Area')
            ->click('Back')
            ->see('Your Districts in Squaretown');
    }

    public function testUpdateDistict()
    {
        $this
            ->see('Test District')
            ->click('Update Test District')
            ->see('Update District')
            ->type('Updated Test District', 'name')
            ->select('tourist', 'type')
            ->press('Update')
            ->see('Your Districts in Squaretown')
            ->see('Updated Test District');
    }

    public function testDeleteDistict()
    {
        $this
            ->see('Test District')
            ->press('Delete Test District')
            ->see('Your Districts in Squaretown')
            ->dontSee('Test District')
            ->see('Inside District Area');
    }
}
