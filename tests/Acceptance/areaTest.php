<?php

namespace App;

use Illuminate\Foundation\Testing\DatabaseMigrations;

class AreaTest extends \TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();

        $this->artisan('db:seed');

        $user = User::where('email', 'Duser@testco.com')->first();

        $this
            ->actingAs($user)
            ->visit('/')
            ->see('Your Districts in Squaretown');
    }

    public function testCreateArea()
    {
        $this
            ->click('Create Area')
            ->see('Create Area')
            ->type('Test Apartment Area', 'name')
            ->select('apartment', 'type')
            ->press('Save')
            ->see('Your Districts in Squaretown')
            ->see('Test Apartment Area');
    }

    public function testVisitArea()
    {
        $this
            ->click('Visit City Hall')
            ->see('City Hall')
            ->click('Back')
            ->see('Your Districts in Squaretown');
    }

    public function testUpdateArea()
    {

        $this
            ->see('Test Area')
            ->click('Update Test Area')
            ->see('Update Area')
            ->type('Test Area - Moved', 'name')
            ->select(1, 'district_id')
            ->see('Test District  - Undeveloped')
            ->select('factory', 'type')
            ->see('Factory')
            ->press('Update')
            ->see('Test Area - Moved')
            ->see('Inside District Area')
            ->click('Back')
            ->dontSee('Test Area - Moved');
    }

    public function testDeleteArea()
    {
        $this
            ->see('Test Area')
            ->press('Delete Test Area')
            ->see('Your Districts in Squaretown')
            ->dontSee('Test Area');
    }
}
